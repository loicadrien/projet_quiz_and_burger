-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 05 fév. 2018 à 22:08
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `quiz`
--

-- --------------------------------------------------------

--
-- Structure de la table `administrateur`
--

DROP TABLE IF EXISTS `administrateur`;
CREATE TABLE IF NOT EXISTS `administrateur` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `administrateur`
--

INSERT INTO `administrateur` (`id_admin`, `nom`, `email`, `password`, `image`) VALUES
(1, 'Loic Youbi', 'yadrienldk14@gmail.com', '9eec4eecae7e71c99bc96ca9794c506205cf0834', 'pp(3).jpg'),
(2, 'Rose Nadia', 'nadkpmbang@gmail.com', '31d6e1db7ed09cf46cd9472f7fff0750a155b72a', 'nadia.jpeg'),
(3, 'Murielle', 'muriellesongue@gmail.com', '4ee5dcef4201d450eaf3cc6c21eb3fb27fb4bc73', 'murielle.jpeg'),
(4, 'Joel Luc', 'joel@gmail.com', '3d116e5c29b7a3f34ceeda4b6ff0979e5535968f', 'joel.jpeg');

-- --------------------------------------------------------

--
-- Structure de la table `chapitre`
--

DROP TABLE IF EXISTS `chapitre`;
CREATE TABLE IF NOT EXISTS `chapitre` (
  `id_chap` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(25) DEFAULT NULL,
  `info` varchar(200) DEFAULT NULL,
  `nbr_lesson` int(11) DEFAULT '0',
  `id_tuto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_chap`),
  KEY `FK_Chapitre_id_tuto` (`id_tuto`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `chapitre`
--

INSERT INTO `chapitre` (`id_chap`, `intitule`, `info`, `nbr_lesson`, `id_tuto`) VALUES
(1, 'Historique', 'Connaitre l\'evolution du langage depuis 1989', 2, 1),
(2, 'Description du langage', 'Syntaxe, Structure, Attributs ...', 0, 1),
(3, 'Introduction', 'le developpement', 1, 2),
(4, 'Les Bases', 'Maitriser les syntaxes du php', 0, 2),
(5, 'Debuter avec le SQL', 'les requÃªtes', 1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `configurerca`
--

DROP TABLE IF EXISTS `configurerca`;
CREATE TABLE IF NOT EXISTS `configurerca` (
  `estComplet` int(11) DEFAULT NULL,
  `id_admin` int(11) NOT NULL,
  `id_chap` int(11) NOT NULL,
  PRIMARY KEY (`id_admin`,`id_chap`),
  KEY `FK_configurerCa_id_chap` (`id_chap`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `configurerca`
--

INSERT INTO `configurerca` (`estComplet`, `id_admin`, `id_chap`) VALUES
(0, 1, 5),
(0, 4, 1),
(0, 4, 2);

-- --------------------------------------------------------

--
-- Structure de la table `configurerl`
--

DROP TABLE IF EXISTS `configurerl`;
CREATE TABLE IF NOT EXISTS `configurerl` (
  `estComplet` int(11) DEFAULT NULL,
  `id_admin` int(11) NOT NULL,
  `id_lesson` int(11) NOT NULL,
  PRIMARY KEY (`id_admin`,`id_lesson`),
  KEY `FK_ConfigurerL_id_lesson` (`id_lesson`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `configurert`
--

DROP TABLE IF EXISTS `configurert`;
CREATE TABLE IF NOT EXISTS `configurert` (
  `estComplet` int(11) DEFAULT NULL,
  `estPret` int(11) DEFAULT NULL,
  `id_tuto` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  PRIMARY KEY (`id_tuto`,`id_admin`),
  KEY `FK_configurerT_id_admin` (`id_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `configurert`
--

INSERT INTO `configurert` (`estComplet`, `estPret`, `id_tuto`, `id_admin`) VALUES
(1, 1, 1, 4),
(0, 1, 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `cours`
--

DROP TABLE IF EXISTS `cours`;
CREATE TABLE IF NOT EXISTS `cours` (
  `id_cours` int(11) NOT NULL AUTO_INCREMENT,
  `textcours` longtext,
  `typeQ` varchar(25) DEFAULT NULL,
  `id_lesson` int(11) DEFAULT NULL,
  `idQCM` int(11) DEFAULT NULL,
  `idQR` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_cours`),
  KEY `FK_Cours_id_lesson` (`id_lesson`),
  KEY `FK_Cours_idQCM` (`idQCM`),
  KEY `FK_Cours_idQR` (`idQR`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `cours`
--

INSERT INTO `cours` (`id_cours`, `textcours`, `typeQ`, `id_lesson`, `idQCM`, `idQR`) VALUES
(1, '       Durant la premiÃ¨re moitiÃ© des annÃ©es 1990, avant lâ€™apparition des technologies web comme JavaScript, les feuilles de style en cascade et le Document Object Model, lâ€™Ã©volution de HTML a dictÃ© lâ€™Ã©volution du World Wide Web. Depuis 1997 et HTML 4, lâ€™Ã©volution de HTML a fortement ralenti ; 10 ans plus tard, HTML 4 reste utilisÃ© dans les pages web. En 2008, la spÃ©cification du HTML5 est Ã  lâ€™Ã©tude.\r\n         HTML est une des trois inventions Ã  la base du World Wide Web, avec le Hypertext Transfer Protocol (HTTP) et les adresses web. HTML a Ã©tÃ© inventÃ© pour permettre d\'Ã©crire des documents hypertextuels liant les diffÃ©rentes ressources dâ€™Internet avec des hyperliens. Aujourdâ€™hui, ces documents sont appelÃ©s Â« page web Â».', 'qcm', 1, NULL, NULL),
(2, ' En particulier, dans la version du 22 aoÃ»t 2012, le document de rÃ©fÃ©rence24 explique que le HTML5 du W3C, publiÃ© le 22 juin 2012, est basÃ© sur une version du HTML Living Standard, mais que le HTML Living Standard ne s\'arrÃªte pas Ã  cette version, et continue Ã  Ã©voluer. Il dÃ©veloppe en particulier les diffÃ©rences entre la version W3C (le HTML5) et la version HTML Living Standard (par exemple, les nouveaux bugs ne sont pas pris en compte dans le HTML5, des diffÃ©rences syntaxiques sont rÃ©pertoriÃ©es, et de nouvelles balises crÃ©Ã©es par le HTML Living Standard ne sont pas incluses dans le HTML5)', 'qcm', 2, NULL, NULL),
(3, 'jhgdhgfjhdgjgsdjffsddfsfsd', 'qcm', 3, NULL, NULL),
(4, 'vbvbvbvbcx', 'qr', 3, NULL, NULL),
(5, 'on verra ici la gÃ©nÃ¨se du sql.\r\nde puis les annÃ©es 90 Ã  aujourd\'hui', 'qcm', 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inscrit`
--

DROP TABLE IF EXISTS `inscrit`;
CREATE TABLE IF NOT EXISTS `inscrit` (
  `dateInscription` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6),
  `pointT` float DEFAULT NULL,
  `etatT` int(11) DEFAULT NULL,
  `estFini` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_tuto` int(11) NOT NULL,
  PRIMARY KEY (`id_user`,`id_tuto`),
  KEY `FK_inscrit_id_tuto` (`id_tuto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `inscrit`
--

INSERT INTO `inscrit` (`dateInscription`, `pointT`, `etatT`, `estFini`, `id_user`, `id_tuto`) VALUES
('2018-01-29 03:00:12.989296', 0, 1, 0, 1, 1),
('2018-01-29 12:35:49.986642', 0, 1, 0, 1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `lesson`
--

DROP TABLE IF EXISTS `lesson`;
CREATE TABLE IF NOT EXISTS `lesson` (
  `id_lesson` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(25) DEFAULT NULL,
  `niv_lesson` float DEFAULT NULL,
  `nbr_cours` int(11) DEFAULT '0',
  `id_chap` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_lesson`),
  KEY `FK_Lesson_id_chap` (`id_chap`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `lesson`
--

INSERT INTO `lesson` (`id_lesson`, `intitule`, `niv_lesson`, `nbr_cours`, `id_chap`) VALUES
(1, 'LeÃ§on 1', NULL, 2, 1),
(2, 'LeÃ§on 2', NULL, 2, 1),
(3, 'LeÃ§on 1', NULL, 3, 3),
(4, 'LeÃ§on 1', NULL, 2, 5);

-- --------------------------------------------------------

--
-- Structure de la table `progresserca`
--

DROP TABLE IF EXISTS `progresserca`;
CREATE TABLE IF NOT EXISTS `progresserca` (
  `pointChap` float DEFAULT NULL,
  `etatCa` int(11) DEFAULT NULL,
  `estFini` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_chap` int(11) NOT NULL,
  PRIMARY KEY (`id_user`,`id_chap`),
  KEY `FK_ProgresserCa_id_chap` (`id_chap`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `progresserca`
--

INSERT INTO `progresserca` (`pointChap`, `etatCa`, `estFini`, `id_user`, `id_chap`) VALUES
(1, 1, 1, 1, 1),
(1, 1, 1, 1, 2),
(0, 1, 0, 1, 5);

-- --------------------------------------------------------

--
-- Structure de la table `progresserco`
--

DROP TABLE IF EXISTS `progresserco`;
CREATE TABLE IF NOT EXISTS `progresserco` (
  `pointCours` float DEFAULT NULL,
  `etatCo` int(11) DEFAULT NULL,
  `estFini` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_cours` int(11) NOT NULL,
  PRIMARY KEY (`id_user`,`id_cours`),
  KEY `FK_ProgresserCo_id_cours` (`id_cours`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `progresserco`
--

INSERT INTO `progresserco` (`pointCours`, `etatCo`, `estFini`, `id_user`, `id_cours`) VALUES
(0, 1, 0, 1, 1),
(0, 1, 0, 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `progresserl`
--

DROP TABLE IF EXISTS `progresserl`;
CREATE TABLE IF NOT EXISTS `progresserl` (
  `pointL` float DEFAULT NULL,
  `etatL` int(11) DEFAULT NULL,
  `estFini` int(1) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL,
  `id_lesson` int(11) NOT NULL,
  PRIMARY KEY (`id_user`,`id_lesson`),
  KEY `FK_ProgresserL_id_lesson` (`id_lesson`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `progresserl`
--

INSERT INTO `progresserl` (`pointL`, `etatL`, `estFini`, `id_user`, `id_lesson`) VALUES
(0, 1, 0, 1, 1),
(0, 1, 0, 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `proposition`
--

DROP TABLE IF EXISTS `proposition`;
CREATE TABLE IF NOT EXISTS `proposition` (
  `idProposition` int(11) NOT NULL AUTO_INCREMENT,
  `libele` varchar(250) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `idQCM` int(11) DEFAULT NULL,
  PRIMARY KEY (`idProposition`),
  KEY `FK_Proposition_idQCM` (`idQCM`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `qcm`
--

DROP TABLE IF EXISTS `qcm`;
CREATE TABLE IF NOT EXISTS `qcm` (
  `idQCM` int(11) NOT NULL AUTO_INCREMENT,
  `libele` varchar(250) DEFAULT NULL,
  `proposition1` varchar(255) NOT NULL,
  `proposition2` varchar(255) NOT NULL,
  `proposition3` varchar(255) NOT NULL DEFAULT 'pas de reponse juste',
  `reponse` int(11) DEFAULT NULL,
  `id_cours` int(11) DEFAULT NULL,
  PRIMARY KEY (`idQCM`),
  KEY `FK_QCM_id_cours` (`id_cours`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `qcm`
--

INSERT INTO `qcm` (`idQCM`, `libele`, `proposition1`, `proposition2`, `proposition3`, `reponse`, `id_cours`) VALUES
(1, 'Quel est la date de crÃ©ation du langage html?', '1992', '1989', '1990', 2, 1),
(2, 'Quel est la derniÃ¨re version du HTML ?', 'html 4', 'html 3', 'html 5', 3, 2),
(3, 'Balise ouverture php?', '<php', '?>', '<?php', 3, 3),
(4, 'En quelle annÃ©e fut l\'apparution du sql?', '1990', '1880', '1992', 1, 5);

-- --------------------------------------------------------

--
-- Structure de la table `qr`
--

DROP TABLE IF EXISTS `qr`;
CREATE TABLE IF NOT EXISTS `qr` (
  `idQR` int(11) NOT NULL AUTO_INCREMENT,
  `libele` varchar(255) DEFAULT NULL,
  `reponse` varchar(250) DEFAULT NULL,
  `id_cours` int(11) DEFAULT NULL,
  PRIMARY KEY (`idQR`),
  KEY `FK_QR_id_cours` (`id_cours`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `qr`
--

INSERT INTO `qr` (`idQR`, `libele`, `reponse`, `id_cours`) VALUES
(1, 'LA balise fermante du php ?', '?>', 4);

-- --------------------------------------------------------

--
-- Structure de la table `tutoriel`
--

DROP TABLE IF EXISTS `tutoriel`;
CREATE TABLE IF NOT EXISTS `tutoriel` (
  `id_tuto` int(11) NOT NULL AUTO_INCREMENT,
  `intituler` varchar(250) DEFAULT NULL,
  `info_sup` varchar(255) NOT NULL,
  `niv` float DEFAULT NULL,
  `nbr_chap` int(11) DEFAULT '0',
  PRIMARY KEY (`id_tuto`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tutoriel`
--

INSERT INTO `tutoriel` (`id_tuto`, `intituler`, `info_sup`, `niv`, `nbr_chap`) VALUES
(1, 'HTML5', 'Hypertext Markup Language', 0, 2),
(2, 'PHP', 'langage de progrmmation', 0, 2),
(3, 'SQL', 'maitriser les requetes', 0, 2),
(4, 'Java', 'Oriente objet', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) DEFAULT NULL,
  `prenom` varchar(250) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `image` varchar(25) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_user`, `nom`, `prenom`, `email`, `password`, `points`, `image`, `active`) VALUES
(1, 'Youbi', 'Loic Adrien', 'Yadrienldk14@gmail.com', '9eec4eecae7e71c99bc96ca9794c506205cf0834', 0, 'pp(2).jpg', 1),
(2, 'Kombang', 'Nadia rose', 'nadkombang@gmail.com', '26df8b08dffb154928fb116889beae515cd974bb', 0, 'nadia.jpeg', 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `chapitre`
--
ALTER TABLE `chapitre`
  ADD CONSTRAINT `FK_Chapitre_id_tuto` FOREIGN KEY (`id_tuto`) REFERENCES `tutoriel` (`id_tuto`);

--
-- Contraintes pour la table `configurerca`
--
ALTER TABLE `configurerca`
  ADD CONSTRAINT `FK_configurerCa_id_admin` FOREIGN KEY (`id_admin`) REFERENCES `administrateur` (`id_admin`),
  ADD CONSTRAINT `FK_configurerCa_id_chap` FOREIGN KEY (`id_chap`) REFERENCES `chapitre` (`id_chap`);

--
-- Contraintes pour la table `configurerl`
--
ALTER TABLE `configurerl`
  ADD CONSTRAINT `FK_ConfigurerL_id_admin` FOREIGN KEY (`id_admin`) REFERENCES `administrateur` (`id_admin`),
  ADD CONSTRAINT `FK_ConfigurerL_id_lesson` FOREIGN KEY (`id_lesson`) REFERENCES `lesson` (`id_lesson`);

--
-- Contraintes pour la table `configurert`
--
ALTER TABLE `configurert`
  ADD CONSTRAINT `FK_configurerT_id_admin` FOREIGN KEY (`id_admin`) REFERENCES `administrateur` (`id_admin`),
  ADD CONSTRAINT `FK_configurerT_id_tuto` FOREIGN KEY (`id_tuto`) REFERENCES `tutoriel` (`id_tuto`);

--
-- Contraintes pour la table `cours`
--
ALTER TABLE `cours`
  ADD CONSTRAINT `FK_Cours_idQCM` FOREIGN KEY (`idQCM`) REFERENCES `qcm` (`idQCM`),
  ADD CONSTRAINT `FK_Cours_idQR` FOREIGN KEY (`idQR`) REFERENCES `qr` (`idQR`),
  ADD CONSTRAINT `FK_Cours_id_lesson` FOREIGN KEY (`id_lesson`) REFERENCES `lesson` (`id_lesson`);

--
-- Contraintes pour la table `inscrit`
--
ALTER TABLE `inscrit`
  ADD CONSTRAINT `FK_inscrit_id_tuto` FOREIGN KEY (`id_tuto`) REFERENCES `tutoriel` (`id_tuto`),
  ADD CONSTRAINT `FK_inscrit_id_user` FOREIGN KEY (`id_user`) REFERENCES `utilisateur` (`id_user`);

--
-- Contraintes pour la table `lesson`
--
ALTER TABLE `lesson`
  ADD CONSTRAINT `FK_Lesson_id_chap` FOREIGN KEY (`id_chap`) REFERENCES `chapitre` (`id_chap`);

--
-- Contraintes pour la table `progresserca`
--
ALTER TABLE `progresserca`
  ADD CONSTRAINT `FK_ProgresserCa_id_chap` FOREIGN KEY (`id_chap`) REFERENCES `chapitre` (`id_chap`),
  ADD CONSTRAINT `FK_ProgresserCa_id_user` FOREIGN KEY (`id_user`) REFERENCES `utilisateur` (`id_user`);

--
-- Contraintes pour la table `progresserco`
--
ALTER TABLE `progresserco`
  ADD CONSTRAINT `FK_ProgresserCo_id_cours` FOREIGN KEY (`id_cours`) REFERENCES `cours` (`id_cours`),
  ADD CONSTRAINT `FK_ProgresserCo_id_user` FOREIGN KEY (`id_user`) REFERENCES `utilisateur` (`id_user`);

--
-- Contraintes pour la table `progresserl`
--
ALTER TABLE `progresserl`
  ADD CONSTRAINT `FK_ProgresserL_id_lesson` FOREIGN KEY (`id_lesson`) REFERENCES `lesson` (`id_lesson`),
  ADD CONSTRAINT `FK_ProgresserL_id_user` FOREIGN KEY (`id_user`) REFERENCES `utilisateur` (`id_user`);

--
-- Contraintes pour la table `proposition`
--
ALTER TABLE `proposition`
  ADD CONSTRAINT `FK_Proposition_idQCM` FOREIGN KEY (`idQCM`) REFERENCES `qcm` (`idQCM`);

--
-- Contraintes pour la table `qcm`
--
ALTER TABLE `qcm`
  ADD CONSTRAINT `FK_QCM_id_cours` FOREIGN KEY (`id_cours`) REFERENCES `cours` (`id_cours`);

--
-- Contraintes pour la table `qr`
--
ALTER TABLE `qr`
  ADD CONSTRAINT `FK_QR_id_cours` FOREIGN KEY (`id_cours`) REFERENCES `cours` (`id_cours`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
