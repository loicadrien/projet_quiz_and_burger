<?php
	// on se connecte a la base de données
		require "database.php";
        extract($_POST);
        $db = Database::connect();
        $req = $db->prepare('SELECT * FROM utilisateur WHERE email = :email AND password = :password');
        $req->execute(array(
            'email' => $_POST['email'],
            'password' => sha1($_POST['password'])));

        $existe = $req->fetch();
        if ($existe) {
            session_start();
            $_SESSION['id_user'] = $existe['id_user'];
            $_SESSION['email'] = $existe['email'];
            $erreur = "Connexion reussi";
            header('Location: ../index.php?error='.$erreur);

        }else{

            $erreur = 'Mot de passe ou email invalide';
            header('Location: ../connexion.php?error='.$erreur); 

        }

?>