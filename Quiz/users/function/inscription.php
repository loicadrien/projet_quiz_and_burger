	<?php
	// on se connecte a la base de données
		require "database.php";
		require "../include/constantes.php";
    $db=database::connect();



		if (isset($_POST['inscription'])){
			 extract($_POST);
       $nomErreur=$prenomErreur=$emailErreur=$passwordErreur=$FileErreur=$passwordconfirmErreur="";

        	$nom = $_POST['nom'];
        	$prenom = $_POST['prenom'];
        	$email = $_POST['email'];
        	$password = $_POST['password'];
        	$confirmpassword = $_POST['confirmpassword'];
   			  $file = checkInput($_FILES["image"]['name']);

          /// print_r(array($_POST, $_FILES));
            $filePath = '../img/'.basename($file);
            $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);
            $isSuccess = true;
            $isUploadSuccess = false;

   		                          //Verification du nom
              if(!preg_match('/^[a-z A-Z]+$/',$nom) || empty($nom)){
                    $nomErreur = "Nom invalide (alphanumerique)";
                    $isSuccess = false;

                }
                 //Verification d' email
                if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                    $emailErreur= "Format d'email invalide";
                    $isSuccess = false;
                    
                }
                else{
                  
                    $req = $db->prepare('SELECT email FROM utilisateur WHERE email = ?');
                    $req->execute(array($email));
                    $user = $req->fetch();
                    if($user){
                        $emailErreur = 'Cet email existe déja';
                        $isSuccess = false;
                    }
                }
                     //Verification du password
                if(strlen($password)<8 || empty($password)){
                    $passwordErreur = "le password invalide";
                    $isSuccess = false;
                  }
                
   					if($password != $confirmpassword)
   					{
   						 $passwordconfirmError = "les mots de passes doivent etre identique";
               $isSuccess = false;
   					}

                                    // controle sur l'image
                      if(empty($file)){
                            $FileError = 'champs image vide';
                            $isSuccess = false;
                        }else{
                            $isUploadSuccess = true;
                            if($fileExtension != "jpg" && $fileExtension != "png" && $fileExtension != "gif" && $fileExtension != "jpeg"){
                                $FileError = "file non autorisés ";
                                $isUploadSuccess = false;

                            }if(file_exists($filePath)){
                                $FileError = "file exist";
                                $isUploadSuccess = false;
                            }if($_FILES['image']["size"] > 500000)
                            {
                                $FileError = "file trop grand ";
                                $isUploadSuccess = false;
                            }if($isUploadSuccess){
                                if(!move_uploaded_file($_FILES['image']["tmp_name"], $filePath)){
                                    $FileError = "upload error ";
                                    $isUploadSuccess = false;
                                }
                            }
                        }
   	
                                                           //Insertion lorsqu'il n'ya pas d"erreurs
                    if(($isSuccess == true && $isUploadSuccess == true)){
                        $req = $db->prepare('INSERT INTO utilisateur SET nom =?,prenom = ?,email=?,password=?,points=?,image=?,active=?');
                        $mat = sha1($password);
                        $req->execute(array($nom,$prenom,$email,$mat,0,$file,1));

                        header("Location: ../connexion.php?erreur=Connexion reussi");

                    }else{
                         header("Location: ../connexion.php?erreur=Des erreurs sont survenues :".$nomErreur."/".$prenomErreur." /".$passwordErreur." /".$emailErreur." /".$FileError."/".$filePath );
                    }
  
   }



  /*

                	// envoi d'un mail d'activation 
                	$to = $email;
                	$subject = NOM_SITE."- ACTIVATION DU COMPTE";
                	$token = sha1($nom.$email.$password);
                	ob_start();
                	require "../template/emails/activation.tmpl.php";
                	$content = ob_get_clean();

                	$header = 'MIME-Version: 1.0'. "\r\n";
                	$header .='content-type: text/html; charset=iso-8859-1'. "\r\n";
                	mail($to, $subject, $content, $header);


*/
                	// informer l'utilisateur pour qu'il verifie sa boite 


   


        	




 function checkInput($data)
{
    $data = trim($data);
    $data = stripcslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

        ?>