<?php 

		 require "database.php";
        extract($_POST);
        $db = Database::connect();

         if(isset($_GET['id_tuto']) && isset($_GET['id_user']) && !isset($_GET['id_chap']))
         {
         		$id_tuto = checkInput($_GET['id_tuto']);
         		$id_user = checkInput($_GET['id_user']);

         		// on verifie si l'utilisateur n'est pas déja inscrit sur ce tuto
         		$req =$db->prepare('SELECT etatT FROM inscrit WHERE id_tuto = ? AND id_user = ?');
         		$req->execute(array($id_tuto,$id_user));
         		$user = $req->fetch();
         		
         		if($user){
         			// si il est déjà inscrit on le renvoi a la page d'index_chap.php
         			header("Location: ../index_chap.php?id_user=".$id_user."&id_tuto=".$id_tuto);
         		}else{
         			//au cas contraire on l'insert dans la bd 
         			$state = $db->prepare("INSERT INTO inscrit SET  pointT=?, etatT=?, estFini=?, id_tuto=?, id_user=?");
         			$state->execute(array(0,1,0,$id_tuto,$id_user));


			
         						// on selectionne  tous les chapitre et on l'inscrit dessus
						$state2 = $db->prepare("SELECT * FROM chapitre WHERE id_tuto = ?");
						$state2->execute([$id_tuto]);
						while($chap = $state2->fetch()) {

		         			$state3 = $db->prepare("INSERT INTO progresserca SET  pointChap=?, etatCa=?, estFini=?, id_chap=?, id_user=?");
		         			$state3->execute(array(0,1,0,(int)$chap['id_chap'],$id_user));

						}
						/*
						//$id_chap = $chap1['id_chap'];

						

	         						// on selectionne la toute premier lecon et on l'inscrit dessus
						$state2 = $bd->prepare("SELECT * FROM lesson WHERE id_chap = ? LIMIT 1");
						$state2 = execute([$id_chap]);
						$lecon1 = $state2->fetch();
						$id_lecon = $lecon1['id_chap'];

         			$state3 = $db->prepare("INSERT INTO progresserl SET  pointL=?, etatL=?, estFini=?, id_user=?, id_lecon=?");
         			$state3>execute(array(0,1,0,$id_user,$id_lecon));
*/

         			header("Location: ../index_chap.php?id_user=".$id_user."&id_tuto=".$id_tuto);
         		}
         	}

         		if(isset($_GET['id_tuto']) && isset($_GET['id_user']) && isset($_GET['id_chap']) && isset($_GET['id_lecon']))
         		{
         		$id_tuto = checkInput($_GET['id_tuto']);
         		$id_user = checkInput($_GET['id_user']);
         		$id_chap = checkInput($_GET['id_chap']);
         		$id_lecon = checkInput($_GET['id_lecon']); 
         			         		// on verifie si l'utilisateur n'est pas déja inscrit sur cette lecon
         		$req =$db->prepare('SELECT etatL FROM progresserl WHERE id_lesson = ? AND id_user = ?');
         		$req->execute(array($id_lecon,$id_user));
         		$lecon = $req->fetch();

         		if($lecon){
         			// si il est déjà inscrit on le renvoi a la page quiz.php
         			header("Location: ../quiz.php?id_user=".$id_user."&id_tuto=".$id_tuto."&id_chap=".$id_chap."&id_lecon=".$id_lecon."&p=1");
         		}else{
         			         			//au cas contraire on l'insert dans la bd 
         			$state = $db->prepare("INSERT INTO progresserl SET  pointL=?, etatL=?, estFini=?, id_user=?, id_lesson=?");
         			$state->execute(array(0,1,0,$id_user,$id_lecon));


			
         						// on selectionne  tous les cours et on l'inscrit dessus
						$state2 = $db->prepare("SELECT * FROM cours WHERE id_lesson = ?");
						$state2->execute([$id_lecon]);
						while($cours = $state2->fetch()) {

		         			$state3 = $db->prepare("INSERT INTO progresserco SET  pointCours=?, etatCo=?, estFini=?, id_user=?, id_cours=?");
		         			$state3->execute(array(0,1,0,$id_user,(int)$cours['id_cours']));

						}
					header("Location: ../quiz.php?id_user=".$id_user."&id_tuto=".$id_tuto."&id_chap=".$id_chap."&id_lecon=".$id_lecon);
         		}

         		}
         		if (isset($_POST['valider'])) {

         			extract($_POST);
         			$selection = checkInput($_POST['q_answer']);
         			$id_user = checkInput($_POST['id_user']);
         			$id_tuto = checkInput($_POST['id_tuto']);
         			$id_chap = checkInput($_POST['id_chap']);
         			$id_lecon = checkInput($_POST['id_lecon']);
         			$id_cours = checkInput($_POST['id_cours']);

         			// on selectionne la reponse juste dans la bd
         			$state = $db->prepare("SELECT reponse from qcm where id_cours = ? ");
         			$state->execute([$id_cours]);
         			$res = $state->fetch();

         			$reponse = (int)$res['reponse'];
         			echo $reponse;
         			
         			$selectionV = (int)$selection;
					echo $selectionV;
         			if($selectionV == $reponse){
         				// on update dab le chapitre
         				$state = $db->prepare("SELECT pointChap from progresserca where id_user = ?");
         				$state->execute([$id_user]);
         				$res = $state->fetch();
         				$pointchap = (int)$res['pointCha'];
         				$pointchap += 1;


         				// on update la table chapitre
         				$req = $db->prepare("UPDATE progresserca set pointChap = ?, estFini = ?");
         				$req->execute([$pointchap,1]);

         				header("Location: ../index_lecon.php?id_user=".$id_user."&id_tuto=".$id_tuto."&id_chap=".$id_chap);
         			}else{

         				header("Location: ../quiz.php?id_user=".$id_user."&id_tuto=".$id_tuto."&id_chap=".$id_chap."&id_lecon=".$id_lecon."p=mauvaise réponse");
         			}
         		}

         

  function checkInput($data)
{
    $data = trim($data);
    $data = stripcslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}


 ?>