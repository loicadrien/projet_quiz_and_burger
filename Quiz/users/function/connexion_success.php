<?php

	session_start();
	require 'database.php';
	$email = $_SESSION['email'];
    $id_user = $_SESSION['id_user'];
	$bd = database::connect();

		// on recupère les information de l'utilisateur
		$statement1 = $bd->prepare("SELECT * FROM `utilisateur` WHERE id_user=?");
		$statement1->execute(array($id_user));
		$user = $statement1->fetch();

		// on selection tout les tutoriel de la bd qui sont complet et pret
		$req = $bd->query("SELECT tutoriel.id_tuto, tutoriel.intituler, tutoriel.info_sup, tutoriel.niv, configurert.estComplet AS complet, configurert.estPret AS pret, configurert.id_tuto FROM tutoriel LEFT JOIN configurert on tutoriel.id_tuto = configurert.id_tuto");

		// on selectionne les tutoriel où les utilisateurs se sont inscrit
	$req1 = $bd->prepare('SELECT tutoriel.id_tuto, tutoriel.intituler,tutoriel.niv, inscrit.etatT as etatT, inscrit.pointT as pointT, inscrit.estFini as estFini, inscrit.id_user, inscrit.id_tuto FROM tutoriel LEFT JOIN inscrit ON tutoriel.id_tuto = inscrit.id_tuto WHERE inscrit.id_user = ?');
		$req1->execute(array($id_user));


			/***** on recupère le id_tuto *****/
	if(isset($_GET['id_tuto']) && isset($_GET['id_user']))
	{	 
	 $id_tuto = checkInput($_GET['id_tuto']);
	 $id_user = checkInput($_GET['id_user']);
				/********************************************************************************************************************/
	 // on veut afficher le tutoriel correspondant
     	$state = $bd->prepare("SELECT tutoriel.id_tuto, tutoriel.intituler,tutoriel.niv, inscrit.etatT as etatT, inscrit.pointT as pointT, inscrit.estFini as estFini, inscrit.id_user, inscrit.id_tuto FROM tutoriel LEFT JOIN inscrit on tutoriel.id_tuto = ? AND inscrit.id_user = ? limit 1");	
		$state->execute(array($id_tuto,$id_user));
		$tuto = $state->fetch();
				/********************************************************************************************************************/
			// on veut lister les chapitres de ce tutoriel en fonction de l'utilisateur
				$state2 = $bd->prepare("SELECT chapitre.id_chap, chapitre.intitule, chapitre.info, chapitre.id_tuto, progresserca.etatCa as etatCa,progresserca.pointChap as pointChap, progresserca.id_chap, progresserca.id_user from chapitre left JOIN progresserca on chapitre.id_chap = progresserca.id_chap WHERE progresserca.id_user = ? AND chapitre.id_tuto = ?");
				$state2->execute(array($id_user,$id_tuto));
				/********************************************************************************************************************/
						if(isset($_GET['id_chap']))
			{
					$id_chap = checkInput($_GET['id_chap']);
				// on achiffe le chapitre correspondant
				$state1 = $bd->prepare("SELECT chapitre.id_chap, chapitre.intitule , progresserca.etatCa as etatCa, progresserca.pointChap as pointCha, progresserca.estFini, progresserca.id_user FROM chapitre LEFT JOIN progresserca on progresserca.id_chap = chapitre.id_chap WHERE chapitre.id_chap = ? AND progresserca.id_user = ?");	
				$state1->execute(array($id_chap,$id_user));
				$chapitre = $state1->fetch();
				/********************************************************************************************************************/
         				// on liste tous les lecons de ce chap
         				$req1 = $bd->prepare("SELECT * FROM lesson WHERE id_chap=?");
         				$req1->execute([$id_chap]);	

						if(isset($_GET['id_lecon'])) {

							$id_lecon = checkInput($_GET['id_lecon']);
											// on achiffe le cours correspondant
						$stat = $bd->prepare("SELECT lesson.id_lesson, lesson.intitule , progresserl.etatL as etatL, progresserl.pointL as pointL, progresserl.estFini, progresserl.id_user FROM lesson LEFT JOIN progresserl on progresserl.id_lesson = lesson.id_lesson WHERE lesson.id_lesson = ? AND progresserl.id_user = ?");

						$stat->execute(array($id_lecon,$id_user));
						$nomLecon = $stat->fetch();

				}
			}
							/*
								// on selectionne le tout premier chapitre
								$state2 = $bd->prepare("SELECT * FROM chapitre WHERE id_tuto = ? LIMIT 1");
								$state2 ->execute([$id_tuto]);
								$chap1 = $state2->fetch();


								// on inscrit l'utilisateur sur le tout premier chapitre dans la table ProgresserCa
								$id_chap = $chap1['id_chap'];
								$req2 = $bd->prepare("INSERT INTO progresserca SET pointChap=?, etatCa=?, estFini=?, id_user=?,id_chap=?");
								$req->execute(array(0,1,0,$id_user,$id_chap));
								// on selectionne tous les chapitre en fonction de son état
								$req = $bd->prepare("SELECT chapitre.intitule, chapitre.id_chap, chapitre.info, chapitre.id_tuto, progresserca.id_chap as idChap, progresserca.id_user as idUser from chapitre left JOIN progresserca on chapitre.id_chap = progresserca.id_chap WHERE progresserca.id_user = 1 AND chapitre.id_tuto = 1"); 
					*/
	
}




  function checkInput($data)
{
    $data = trim($data);
    $data = stripcslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
