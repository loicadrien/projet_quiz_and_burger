

<!-- on liste les cours deja commencé donc etat du tuto est egale a 1-->
  	<div class="row">
  		<div class="col-md-2 col-sx-12 container" >
  			  <h2>Mes cours</h2>
  			
  		</div>
  		 <div class="col-md-10 col-sx-12 container" >
 				
				<div class="row">
					<?php 
						while ($inscrit = $req1->fetch()) {
							
							if($inscrit['etatT'] == 1){
								
								?>
							 <div class="col-md-3 col-sx-6">
								<div class="card text-center bg-secondary" style="width: 15rem;">
								  <div class="card-block">
									 <h3 class="card-title"><?php  echo $inscrit['intituler']; ?></h3>
							   
							    <h1 class="card-text "><?php echo $inscrit['niv']; ?> pts</h1>
							    <a href="index_chap.php?id_user=<?php echo $id_user; ?>&amp;id_tuto=<?php echo $inscrit['id_tuto']; ?>" class="btn btn-info ">continuer</a>
							  </div>
							</div>
					  </div>
				<?php
							}
						}

					 ?>
				</div>	 
  		</div>
  		
  	</div>
  	<hr>


    <!-- Portfolio -->
    <div class="row" id="portfolio">
      <div class="container">
        <div class="content-section-heading text-center">
          <h3 class="text-secondary mb-0	">Tutoriels</h3>
          <h2 class="mb-5">Récemment Ajouter</h2>
        </div>
        <div class="row no-gutters">
        <?php 
			while ($tuto = $req->fetch()) {
							
				if($tuto['pret'] == 1){
				?>
          <div class="col-md-3 offset-1 col-sx-6">
            <a class="portfolio-item" href="function/function.php?id_user=<?= $id_user ?>&amp;id_tuto=<?php echo $tuto['id_tuto']; ?>">
              <span class="caption">
                <span class="caption-content">
                  <h1><?php echo $tuto['intituler']; ?></h1>
                  <p class="mb-3" style=" width: 10em; height: 10em; overflow: hidden;text-overflow: ellipsis;"><?php echo $tuto['info_sup']; ?></p>
                </span>
              </span>
              <img class="img-fluid" src="img/portfolio-1.jpg" alt="">
            </a>
          </div>
          				<?php
							}
						}
					 ?>
        </div>
      </div>
    </div>
    <hr>