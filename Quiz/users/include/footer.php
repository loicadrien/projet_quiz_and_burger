        <!-- Footer -->
    <footer class="footer text-center" id="contact">
      <div class="container">
        <ul class="list-inline mb-5">
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="#">
              <i class="icon-social-facebook"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="#">
              <i class="icon-social-twitter"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white" href="#">
              <i class="icon-social-instagram"></i>
            </a>
          </li>
        </ul>
        <p class="text-muted small mb-0">Copyright &copy; LearnQuiz 2017</p>
      </div>
    </footer>

  <!-- The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Inscription</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
             <form action="function/inscription.php" method="post" class="r-form" enctype="multipart/form-data" >

                <div class="form-group">
                  <label for="nom">nom</label>
                  <input type="text" class="form-control" id="nom" name="nom">
                </div>
                <div class="form-group">
                  <label for="prenom">prenom</label>
                  <input type="text" class="form-control" id="prenom" name="prenom">
                </div>
                <div class="form-group">
                  <label for="email">email</label>
                  <input type="text" class="form-control" id="email" name="email">
                </div>
                <div class="form-group">
                  <label for="pwd">Password</label>
                  <input type="password" class="form-control" id="password" name="password">
                </div>
                 <div class="form-group">
                  <label for="confirmpassword">Confirmer Password</label>
                  <input type="password" class="form-control" id="confirmpassword" name="confirmpassword">
                </div>
                   <div class="form-group">
                        <input type="file" id="image" name="image" class="form-control">
                       
                  </div>
               <button class="btn btn-dark btn-xl js-scroll-trigger" type="submit" name="inscription" value="inscription">Inscription</button>

            </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
