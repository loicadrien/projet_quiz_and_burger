    <!-- Header -->
    <header class="masthead" style="height: 255px;">
     
      <div class="container text-center">

        <h1 class="mb-1">SoNaLaJo</h1>
        <h3 class="mb-5">
          <em>Apprenez facilement vos leçons à partir de notre système de quiz</em>
        </h3>
        <a class="btn btn-primary btn-xl js-scroll-trigger" href="#connexion">Continuer !</a>
      </div>
      <div class="overlay"></div>
    </header>
    <hr>