

<!-- on liste les cours deja commencé donc etat du tuto est egale a 1-->
  	<div class="row text-center text-primary">
      
  		 <div class="col-md-10 col-sx-12 container" >
 				 <h2 class="">Liste des chapitres </h2>
				<div class="row">
					<?php 
						while ($chap = $state2->fetch()) {
							
							if($chap['etatCa'] == 1){
                           echo '  <div class="col-md-3 text-center offset-1">
                            <div class="card flex-md-row mb-4 box-shadow h-md-150">
                              <div class="card-body d-flex flex-column ">';
                           echo '<strong class="d-inline-block mb-2 text-success">Chapitre N°: '.$chap['id_chap'].' </strong>';
                           echo '<h3 class="mb-0">
                                  <a class="text-dark" href="#">'.$chap['intitule'].'</a>
                                </h3>';
                           echo '<div class="mb-1 text-muted"></div>';
                           echo '<p class="card-text mb-auto">'.$chap['info'].'</p>';
                           echo '                     <br>
                              <p class="card-text mb-auto"> <span class="w4-badge w3-jumbo w9-padding w9-black">'.$chap['pointChap'].'pts</span></p>
                        <br>
                              <p class="card-text text-white"> <a type="button" href="index_lecon.php?id_user='.$id_user.'&amp;id_tuto='.$id_tuto.'&id_chap='.$chap['id_chap'].'" class="btn btn-info">Commencer</a></p>
                            </div>
                          </div>
                        </div>'; 
								 
								?>

				<?php
							}
						}

					 ?>
				</div>	 
  		</div>
  		
  	</div>
  	<hr>
