<?php
session_start();
session_destroy();
$erreur="";
if (isset($_GET['error'])) {
  $erreur = $_GET['error'];
}
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Learn Quiz</title>

    <!-- Bootstrap Core CSS -->
   
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.min.css" rel="stylesheet">

  </head>

  <body id="page-top">
    <!-- Navigation -->
    <a class="menu-toggle rounded" href="#">
      <i class="fa fa-bars"></i>
    </a>
    <nav id="sidebar-wrapper">
      <ul class="sidebar-nav">
        <li class="sidebar-brand">
          <a class="js-scroll-trigger" href="#">Menu</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="#">Accueil</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="#connexion">Connexion</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" data-toggle="modal" data-target="#myModal"">Inscription</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="../administration/connexion.php">Vous etes admin</a>
        </li>
        <li class="sidebar-nav-item">
          <a class="js-scroll-trigger" href="#contact">Contact</a>
        </li>
      </ul>
    </nav>

    <!-- Header -->
    <header class="masthead d-flex">
      <div class="container text-center my-auto">
        <h1 class="mb-1">Learn quiz</h1>
        <h3 class="mb-5">
          <em>Apprenez facilement vos leçons à partir de notre système de quiz</em>
        </h3>
        <a class="btn btn-primary btn-xl js-scroll-trigger" href="#connexion">Commencer !</a>
      </div>
      <div class="overlay"></div>
    </header>

    <!-- About -->
    <section class="content-section bg-light" id="connexion">
      <div class="container text-center">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h5 class="text-warning"><?= $erreur ?></h5>
            <h2> Connexion </h2>
              <form method="POST" action="function/connexion.php">
                <div class="form-group">
                  <label for="email">email</label>
                  <input type="text" class="form-control" id="email" name="email">
                </div>
                <div class="form-group">
                  <label for="pwd">Password</label>
                  <input type="password" class="form-control" id="password" name="password">
                </div>
             <button class="btn btn-dark btn-xl js-scroll-trigger" type="submit" >Connexion</button>

            </form>
          </div>


          
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer class="footer text-center" id="contact">
      <div class="container">
        <ul class="list-inline mb-5">
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="#">
              <i class="icon-social-facebook"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="#">
              <i class="icon-social-twitter"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white" href="#">
              <i class="icon-social-instagram"></i>
            </a>
          </li>
        </ul>
        <p class="text-muted small mb-0">Copyright &copy; LearnQuiz 2017</p>
      </div>
    </footer>

  <!-- The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Inscription</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
             <form action="function/inscription.php" method="post" class="r-form" enctype="multipart/form-data" >

                <div class="form-group">
                  <label for="nom">nom</label>
                  <input type="text" class="form-control" id="nom" name="nom">
                </div>
                <div class="form-group">
                  <label for="prenom">prenom</label>
                  <input type="text" class="form-control" id="prenom" name="prenom">
                </div>
                <div class="form-group">
                  <label for="email">email</label>
                  <input type="text" class="form-control" id="email" name="email">
                </div>
                <div class="form-group">
                  <label for="pwd">Password</label>
                  <input type="password" class="form-control" id="password" name="password">
                </div>
                 <div class="form-group">
                  <label for="confirmpassword">Confirmer Password</label>
                  <input type="password" class="form-control" id="confirmpassword" name="confirmpassword">
                </div>
                   <div class="form-group">
                        <input type="file" id="image" name="image" class="form-control">
                       
                  </div>
               <button class="btn btn-dark btn-xl js-scroll-trigger" type="submit" name="inscription" value="inscription">Inscription</button>

            </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/stylish-portfolio.min.js"></script>

  </body>

</html>
