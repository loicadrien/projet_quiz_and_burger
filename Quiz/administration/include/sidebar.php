<?php require 'function/nbr_admin.php'; ?>
        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header text-center">
                   <img src="image/<?php echo $admin['image']; ?>" alt="Avatar"  style="width:50%;height: 50px; margin-left: 10px; border-radius: 50%;"> <h3><?php  echo $admin['nom'] ?></h3>
                   <h5> <?php  echo $admin['email'] ?></h5>
                    
                </div>

                <ul class="list-unstyled container ">
                    <hr>
                    <li>
                       
                        <button class="btn btn-info" data-target="#pageSubmenu1" data-toggle="collapse">
                            Tuto
                             <span class="badge w3-badge"><?= $tutoriel['Nbrtuto'] ?></span>
                        </button>
                        <ul class="collapse list-unstyled" id="pageSubmenu1">
                            <?php while ($tutori = $state->fetch()) { ?>
                                <li><a href="listeTuto.php?id_tuto=<?= $tutori['id_tuto']; ?>"><?= $tutori['intituler']; ?></a></li>


                            <?php }  ?>
                        </ul>
                    </li>



                    <hr>
                    <li>
                        
                        <button class="btn btn-info" data-target="#pageSubmenu" data-toggle="collapse">
                            administrateurs
                             <span class="badge w3-badge"><?php echo $admin1['Nbradmin']; ?></span>
                        </button>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <?php while ($administrateur = $state2->fetch()) { ?>
                            <li><a href="#"><?= $administrateur['nom']; ?></a></li>


                            <?php }  ?>
                            
                          
                        </ul>
                    </li>
                                     <hr>
                    <li>
                       
                        <button class="btn btn-info" data-target="#pageSubmenu2" data-toggle="collapse">
                            Utilisateurs
                             <span class="badge w3-badge"><?= $user['Nbruser']?></span>
                        </button>
                        <ul class="collapse list-unstyled" id="pageSubmenu2">
                            <?php while ($utilisateur = $state1->fetch()) { ?>
                                <li><a href="#"><?= $utilisateur['nom']; ?></a></li>


                            <?php }  ?>
                        </ul>
                    </li>
                    <hr>
                    <li>
                        <a href="function/deconnexion.php">
                             Deconnexion
                        </a>
                    </li>
                     <hr>
                    <li>
                        <a href="addAdmin.php">
                           Inscrire un admin
                        </a>
                    </li>
                                         <hr>
                    <li>
                        <a href="addUser.php">
                           Inscrire un utilisateur
                        </a>
                    </li>
                    <hr>
                    <li>
                    </li>
                </ul>

            </nav>
