<div class="content">
	              <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    	  <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-left"></i>
                                <span><--></span>
                            </button>
                        </div>

                          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Tutoriels</a></li>
                                <li><a href="#">Chapitres</a></li>
                                <li><a href="#">Cours</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
              <h2><a href="index.php">../</a> </h2>
                <p>L'insertion des elements dans la base de données se fait a partir d'un carnevas de questionnaire. vous devez donc remplir souagneusement les champs</p>
	<!-- ici on affiche le nom du tuto et son intituler-->

	<h1 class="text-center"><?php echo $tuto['intituler']  ?> </h1>
	<form method="post" class="form text-center" style="width:200" action="function/function.php">

		<div class="form-group">
			<label class="text-center" for="info">Intituler</label>
			<input type="text" name="intituler" class="form-control" value="<?php echo $tuto['intituler']  ?>">
			<input type="text" name="id_tuto" class="form-control" style="display: none;" value="<?php echo $tuto['id_tuto']  ?>">
		</div>
			<div class="form-group">
				<label class="text-center" for="info"> Information sur le tuto</label>
				<textarea class="form-control text-justify" rows="5" cols="10" name="info" ><?php echo $tuto['info_sup']  ?> </textarea>
			</div> 
			<div class="text-center">
				<input type="submit" class="btn btn-primary" value="Update le tutoriel" name="updateTuto">
			</div>

	</form>
	<hr>
		<h1 class="text-center">Les chapitres </h1>

		   <div class="table-responsive">
            <table class="table table-striped">
              <thead class="text-center">
                <tr>
                  <th>#</th>
                  <th>Intituler</th>
                  <th>Information sur le chapitre</th>
                  <th>nombre de lecon</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody class="text-center">
              		<?php 

					foreach ($listT as $list) {
						
						echo '<tr >';
						echo "<td>".$list['id_chap']."</td>";
						echo "<td>".$list['intitule']."</td>";
						echo "<td>".$list['info']."</td>";
						echo "<td>".$list['nbr_lesson']."</td>";
						echo '<td width=300><div class="navbar-tool"><a class="btn btn-primary" href="modifierChap.php?id_chap='.$list['id_chap'].'&id_tuto='.$tuto['id_tuto'] .'"><span class="glyphicon glyphicon-pencil"></span> Modifier</a><a class="btn btn-danger" href="delete.php?id="><span class="glyphicon glyphicon-remove"></span> Supprimer</a></div></td>';
						echo '</tr>';
		
					}
					 ?>
              </tbody>
          </table>
	</div>
</div>
            		
