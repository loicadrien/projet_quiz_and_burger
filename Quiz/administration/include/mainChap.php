  <!-- Page Content Holder -->
<div id="content">
              <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    	  <div class="navbar-header">
                            <button type="button"  class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-left"></i>
                                <span><--></span>
                            </button>
                        </div>

                          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Tutoriels</a></li>
                                <li><a href="#">Chapitres</a></li>
                                <li><a href="#">Cours</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>

              <h2><a href="index.php">../</a><a href="modifTuto.php?id_tuto=<?php echo $id_tuto ?>"><em class="text-primary"><?php echo $tuto['intituler'] ?></em></a>/<em class="text-success"><?php echo $chap['intitule']?> </em></h2>
                <p>L'insertion des elements dans la base de données se fait a partir d'un carnevas de questionnaire. vous devez donc remplir souagneusement les champs</p>

            <div class="row">
            	
            	 <?php
				       	            		// on select tous les tuto et on les affiche
							$state = $bd->prepare("SELECT * FROM lesson  WHERE id_chap = ?");
							$state->execute(array($id_chap));
							while ($lesson = $state->fetch()) {

																          				// on selectionne les champs estPret et estComplet de configurert 
			            			$req = $bd->prepare("SELECT configurert.id_tuto, configurert.estPret, configurert.estComplet FROM configurert WHERE configurert.id_tuto = ? ");
			            			$req->execute(array($id_tuto));
			            			$conf = $req->fetch();
            			if ($conf['estPret'] == 0) {
									echo '  <div class="col-md-3 text-center">
									          <div class="card flex-md-row mb-4 box-shadow h-md-150">
									           <div><a href="function/function.php?id_tutoD='.$id_tuto.'&id_chapD='.$id_chap.'&id_lessonD='.$lesson['id_lesson'].'"><img src="image/delete.png"></a></div>
									            <div class="card-body d-flex flex-column ">';
									         echo '<strong class="d-inline-block mb-2 text-success">Lesson N°: '.$lesson['id_lesson'].' </strong>';
									         echo '<h3 class="mb-0">
									                <a class="text-dark" href="#">'.$lesson['intitule'].'</a>
									              </h3>';
									         echo '<div class="mb-1 text-muted"></div>';
									         echo '				              <br>
								              <p class="card-text mb-auto"> <span class="w4-badge w3-jumbo w9-padding w9-black">'.$lesson['nbr_cours'].'</span></p>
												<br>
								              <p class="card-text text-white"> <a type="button" href="modifLesson.php?id_tuto='.$id_tuto.'&id_chap='.$id_chap.'&id_lesson='.$lesson['id_lesson'].'" class="btn btn-info">Modifier</a></p>
								            </div>
								          </div>
								        </div>';
								    }else if ($conf['estPret'] == 1 && $conf['estComplet'] == 0) {
										echo ' <div class="col-md-3 text-center">
									          <div class="card bg-warning flex-md-row mb-4 box-shadow h-md-150">
									          <div><a href="function/function.php?id_tutoD='.$id_tuto.'&id_chapD='.$id_chap.'&id_lessonD='.$lesson['id_lesson'].'"><img src="image/delete.png"></a></div>

									            <div class="card-body d-flex flex-column ">';
									         echo '<strong class="d-inline-block mb-2 text-success">Lesson N°: '.$lesson['id_lesson'].' <span class="badge badge-danger" style="margin-left: 25px;" data-toggle="tooltip" title="le tutoriel de ce chapitre est pres pour ca mise en ligne" data-placement="right">ready!</span> </strong>';
									         echo '<h3 class="mb-0">
									                <a class="text-dark" href="#">'.$lesson['intitule'].'</a>
									              </h3>';
									         echo '<div class="mb-1 text-muted"></div>';
									         echo '				              <br>
								              <p class="card-text mb-auto"> <span class="w4-badge w3-jumbo w9-padding w9-black">'.$lesson['nbr_cours'].'</span></p>
												<br>
								              <p class="card-text text-white"> <a type="button" href="modifLesson.php?id_tuto='.$id_tuto.'&id_chap='.$id_chap.'&id_lesson='.$lesson['id_lesson'].'" class="btn btn-info">Consulter</a></p>
								            </div>
								          </div>
								        </div>';


									}else if ($conf['estComplet'] == 1) {
										echo '  <div class="col-md-3 text-center">
									          <div class="card bg-success flex-md-row mb-4 box-shadow h-md-150">
									            <div class="card-body d-flex flex-column ">';
									         echo '<strong class="d-inline-block mb-2 text-success">Lesson N°: '.$lesson['id_lesson'].' <span class="badge badge-danger" style="margin-left: 25px;" data-toggle="tooltip" title="le tutoriel de ce chapitre est pres pour ca mise en ligne" data-placement="right">online!</span> </strong>';
									         echo '<h3 class="mb-0">
									                <a class="text-dark" href="#">'.$lesson['intitule'].'</a>
									              </h3>';
									         echo '<div class="mb-1 text-muted"></div>';
									         echo '				              <br>
								              <p class="card-text mb-auto"> <span class="w4-badge w3-jumbo w9-padding w9-black">'.$lesson['nbr_cours'].'</span></p>
												<br>
								              <p class="card-text text-white"> <a type="button" href="modifLesson.php?id_tuto='.$id_tuto.'&id_chap='.$id_chap.'&id_lesson='.$lesson['id_lesson'].'" class="btn btn-info">Consulter</a></p>
								            </div>
								          </div>
								        </div>';


									}		
								}


				          ?>


				       

				              
				              
				              

				           <?php 
				          		// on selectionne les champs estPret et estComplet de configurert 
			            			$req = $bd->prepare("SELECT configurert.id_tuto, configurert.estPret, configurert.estComplet FROM configurert WHERE configurert.id_tuto = ? ");
			            			$req->execute(array($id_tuto));
			            			$conf = $req->fetch();

			            			   		if ($conf['estPret'] == 0) { ?>
			            			
					            	<div class="col-sx-3 ">
					            		<div class="card text-black bg-default mb-3 text-center" style="width: 15rem;">
											  <div class="card-header">Lesson</div>
												  <div class="card-body">
												    <p class="card-text text-white"><a type="button" class="btn btn-info" href="function/function?id_tuto=<?php echo $id_tuto ?>&id_chap=<?php echo $id_chap ?>" >Ajouter</a></p>
											 	  </div>
										</div>

					            	</div>
			            		<?php 
			            	}else if ($conf['estPret'] == 1 && $conf['estComplet'] == 0) { ?>
			            			
					            	<div class="col-sx-3 ">
					            		<div class="card text-black bg-warning mb-3 text-center" style="width: 15rem;">
											  <div class="card-header">Lesson</div>
												  <div class="card-body">
												    <p class="card-text text-white"><a type="button" class="btn btn-info" href="function/function?id_tuto=<?php echo $id_tuto ?>&id_chap=<?php echo $id_chap ?>" >Ajouter</a></p>
											 	  </div>
										</div>

					            	</div>
			            		<?php 
			            	}else if ($conf['estComplet'] == 1) { ?>
			            			
					            	<!--div class="col-sx-3 ">
					            		<div class="card text-black bg-default mb-3 text-center" style="width: 15rem;">
											  <div class="card-header">Lesson</div>
												  <div class="card-body">
												    <p class="card-text text-white"><a type="button" class="btn btn-info" href="function/function?id_tuto=<?php echo $id_tuto ?>&id_chap=<?php echo $id_chap ?>" >Ajouter</a></p>
											 	  </div>
										</div>

					            	</div-->
			            		<?php 
			            	}

			            			?>



            </div>


	</div>

</div>

