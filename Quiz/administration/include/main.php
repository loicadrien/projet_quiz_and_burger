  <!-- Page Content Holder -->
<div id="content">
              <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    	  <div class="navbar-header">
                            <button type="button" class="btn btn-info navbar-btn">
                            
                                <span><--></span>
                            </button>
                        </div>

                          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Tutoriels</a></li>
                                <li><a href="#">Chapitres</a></li>
                                <li><a href="#">Cours</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>

              
                <p>L'insertion des elements dans la base de données se fait a partir d'un carnevas de questionnaire. vous devez donc remplir souagneusement les champs</p>

            <div class="row">
            	<?php
            		// on select tous les tuto et on les affiche
					$state = $bd->query("SELECT * FROM tutoriel ");				
            		while ($tuto = $state->fetch()) {
            				// on selectionne les champs estPret et estComplet de configurert 
            			$req = $bd->prepare("SELECT configurert.id_tuto, configurert.estPret, configurert.estComplet FROM configurert WHERE configurert.id_tuto = ? ");
            			$req->execute(array($tuto['id_tuto']));
            			$conf = $req->fetch();

					if($conf['estPret'] == 0){

						        	echo '<div class="col-sx-3 offset-1">
		            		<div class="card text-white bg-secondary mb-3 text-center" style="width: 15rem;">';
								echo  '<div class="card-header">'. $tuto['intituler'].'</div>
									  	<div class="card-body">';
									  	echo '<div style=" width: 13em; height: 10em; overflow: hidden;text-overflow: ellipsis;">';
									    	echo '<h5 class="card-title">'. $tuto['info_sup'].'</h5>';
									    	echo '</div>';
									    echo '<p class="card-text text-white"> <a type="button" href="modifTuto.php?id_tuto='.$tuto['id_tuto'].'" class="btn btn-info">Consulter</a></p>
								  	   </div>
							</div>

		            </div>';
					}else if ($conf['estPret'] == 1 && $conf['estComplet'] == 0) {
							echo '<div class="col-sx-3 offset-1">
		            		<div class="card text-white bg-warning mb-3 text-center" style="width: 15rem;">';
								echo  '<div class="card-header">'. $tuto['intituler'].'<a href="function/function.php?id_tutoF='.$tuto['id_tuto'].'" style="margin-left: 25px;"><span class="badge badge-danger" data-toggle="tooltip" title="Le tuto est pret. cliquer pour le mettre en ligne Attention une fois le tuto en ligne vous ne pouvez plus le modifier ! " data-placement="right">ready!</span></a></div>
									  	<div class="card-body">';
									  	echo '<div style=" width: 13em; height: 10em; overflow: hidden;text-overflow: ellipsis;">';
									    	echo '<h5 class="card-title">'. $tuto['info_sup'].'</h5>';
									    	echo '</div>';
									    echo '<p class="card-text text-white"> <a type="button" href="modifTuto.php?id_tuto='.$tuto['id_tuto'].'" class="btn btn-default">consulter</a></p>
								  	   </div>
							</div>

		            </div>';
					}else if($conf['estComplet'] == 1){

						echo '<div class="col-sx-3 offset-1">
		            		<div class="card text-white bg-success mb-3 text-center" style="width: 15rem;">';
								echo  '<div class="card-header">'. $tuto['intituler'].'<a href="#" style="margin-left: 25px;"><span class="badge badge-warning" data-toggle="tooltip" title="Le tuto est en ligne" data-placement="right">online</span></a></div>
									  	<div class="card-body">';
									  	echo '<div style=" width: 13em; height: 10em; overflow: hidden;text-overflow: ellipsis;">';
									    	echo '<h5 class="card-title">'. $tuto['info_sup'].'</h5>';
									    	echo '</div>';
									    echo '<p class="card-text text-white"> <a type="button" href="modifTuto.php?id_tuto='.$tuto['id_tuto'].'" class="btn btn-default">Consulter</a></p>
								  	   </div>
							</div>

		            </div>';
					}
		
		          
            			}
            			
					?>

		            	<div class="col-sx-3 offset-1">
		            		<div class="card text-white bg-secondary mb-3 text-center" style="width: 15rem;">
								  <div class="card-header">Tutoriel</div>
									  <div class="card-body">

									    	<a href="#panel1" data-toggle="modal" data-target="#add_tuto"><div class="card-title plus" >+</div></a> 
									    <p class="card-text text-white"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#add_tuto">Ajouter</button></p>
								 	  </div>
							</div>

		            	</div>


            </div>

            				              <!-- The Modal -->
				  <div class="modal fade" id="add_tuto">
				    <div class="modal-dialog modal-sm">
				      <div class="modal-content">
				      
				        <!-- Modal Header -->
				        <div class="modal-header">
				          <h4 class="modal-title">Ajouter un tutoriel </h4>
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				        </div>
				        
				        <!-- Modal body -->
				        <div class="modal-body">
				        	<form method="POST" action="function/function.php">
							       <div class="form-group">
								    <label for="tuto_name">Intituler du tuto</label>
								    <input type="text" class="form-control" id="tuto_name" name="tuto_name">
								  </div>

								  <div class="form-group">
								    <label for="info_tuto">Information sur le tuto</label>
								    <textarea class="form-control" rows="4" id="info_tuto" name="tuto_info" required></textarea>
								  </div>
								  <button type="submit" class="btn btn-primary" name="addTuto" value="Ajouter" required>Ajouter</button>
						   </form>

				        </div>
				        
				        <!-- Modal footer -->
				        <div class="modal-footer">
				          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        </div>
				        
				      </div>
				    </div>
				  </div>

	</div>

</div>

