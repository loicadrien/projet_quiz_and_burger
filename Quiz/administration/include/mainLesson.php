  <!-- Page Content Holder -->
<div id="content">
              <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    	  <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-left"></i>
                                <span><--></span>
                            </button>
                        </div>

                          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Tutoriels</a></li>
                                <li><a href="#">Chapitres</a></li>
                                <li><a href="#">Cours</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <h1> <?php echo $message; ?></h1>

              <h2><a href="index.php">../</a><a href="modifTuto.php?id_tuto=<?php echo $id_tuto ?>"><em class="text-primary"><?php echo $tuto['intituler'] ?></em></a>/<a href="modifChap.php?id_tuto=<?php echo $id_tuto ?>&id_chap=<?php echo $id_chap ?>"><em class="text-success"><?php echo $chap['intitule']?></em></a>/<em class="text-secondary"><?php echo $lesson['intitule']?></em></h2>
                <p>L'insertion des elements dans la base de données se fait a partir d'un carnevas de questionnaire. vous devez donc remplir souagneusement les champs</p>

            <div class="row">
            	
            	 <?php
				       	            		// on select tous les cours et on les affiche
							$state = $bd->prepare("SELECT * FROM cours  WHERE id_lesson = ? ");
							$state->execute(array($id_lesson));
							while ($cour = $state->fetch()) {
									echo '  <div class="col-md-3 text-center">
									          <div class="card flex-md-row mb-4 box-shadow h-md-150">
									            <div class="card-body d-flex flex-column ">';
									         echo '<strong class="d-inline-block mb-2 text-success">Cours N°: '.$cour['id_cours'].' </strong>';
									         echo '<h3 class="mb-0">
									                <a class="text-dark" style="width: 13em; height: 5em; overflow: hidden; text-overflow: ellipsis;" href="#">'.$cour['textcours'].'</a>
									              </h3>';
									         
									         echo '<br>
								              
												<br>';
												if ($cour['typeQ'] == "qcm") {

													$stat = $bd->prepare("SELECT * FROM qcm  WHERE id_cours = ?");
													$stat->execute(array($cour['id_cours']));
													$qcm = $stat->fetch();
													echo '<strong><span class="badge badge-danger" data-toggle="tooltip" title="Q.'.$qcm['libele'].'1. '.$qcm['proposition1'].'  2. '.$qcm['proposition2'].'  3. '.$qcm['proposition3'].'  R.'.$qcm['reponse'].'"';



												}else if ($cour['typeQ'] == "qr") {

													$stat3 = $bd->prepare("SELECT * FROM qr WHERE id_cours = ?");
													$stat3->execute(array($cour['id_cours']));
													$qr = $stat3->fetch();
												echo ' <strong><span class="badge badge-info " data-toggle="tooltip" title="Q.'.$qr['libele'].'R. '.$qr['reponse'].'"';			

											}
								               echo ' data-placement="right">Voir les questions</span></strong>';
								            echo '</div>
								          </div>
								        </div>';

								 echo  '<div class="modal fade" id="myModal">
										    <div class="modal-dialog">
										      <div class="modal-content">
								      
								        <!-- Modal Header -->
								        <div class="modal-header">';

										$stat = $bd->prepare("SELECT typeQ FROM cours  WHERE id_cours = ? ");
										$stat->execute(array($cour['id_cours']));
										$type = $stat->fetch();

								              if($type['typeQ'] == "qcm"){
								              	echo ' <h4 class="modal-title">Question type QCM</h4>';
								              	/*
								              	echo '<button type="button" class="close" data-dismiss="modal">&times;</button>
								          </div>';

								              	echo '  <!-- Modal body -->
      													  <div class="modal-body">';
								              		$stat = $bd->prepare("SELECT * FROM qcm  WHERE id_cours = ?");
													$stat->execute(array($cour['id_cours']));
													$qcm = $stat->fetch();
													 
														echo '<h4 class="text-sm">'.$qcm['libele'].'</h4>';
														echo ' <li>'.$qcm['proposition1'].'</li>';
														
														echo ' <li>'.$qcm['proposition2'].'</li>';
														
														echo '<li>'.$qcm['proposition3'].'</li>';
																
													echo '<h3> Reponse : '. $qcm['reponse'].'</h3>';
													*/

													echo'</div>';
								              }
								              if($type['typeQ'] == "qr"){

								              		echo ' <h4 class="modal-title">Question type QR</h4>';
								              		/*
								              			echo '<button type="button" class="close" data-dismiss="modal">&times;</button>
								          </div>';
								              		$stat = $bd->prepare("SELECT * FROM qr WHERE id_cours = ?");
													$stat->execute(array($cour['id_cours']));
													$qr = $stat->fetch();
													echo '  <!-- Modal body -->
      													  <div class="modal-body">';
													  echo '<div class="alert alert-dark">
													  			<strong>Question : </strong>'. $qr['libele'];
														echo '</div>';
														echo '<h3> Reponse : '. $qr['reponse'].'</h3>';
														echo'</div>';
														*/
												echo'</div>';
								              }
 													 

 													    echo '   <!-- Modal footer -->
														        <div class="modal-footer">
														          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
														        </div>
														        </div>
														       </div>';
 													 	echo '</div>';	
 													 	
										
									}

	


				          ?>

				                   <?php 
				          		// on selectionne les champs estPret et estComplet de configurert 
			            			$req = $bd->prepare("SELECT configurert.id_tuto, configurert.estPret, configurert.estComplet FROM configurert WHERE configurert.id_tuto = ? ");
			            			$req->execute(array($id_tuto));
			            			$conf = $req->fetch();

			            			   		if ($conf['estPret'] == 0) { ?>
			      		            	
			      		            	<div class="col-sx-3">
					            		<div class="card text-black bg-default mb-3 text-center" style="width: 15rem;">
											  <div class="card-header">Ajouter un Cours</div>
												  <div class="card-body">

												    <p class="card-text text-white"><button type="button" class="btn btn-info" href="" data-toggle="modal" data-target="#add_cour">Ajouter !</button></p>
											 	  </div>
										</div>

		            				 </div>
			            		<?php 
			            	}else if ($conf['estPret'] == 1 && $conf['estComplet'] == 0) { ?>
			            			
			            	<div class="col-sx-3">
		            		<div class="card text-black bg-warning mb-3 text-center" style="width: 15rem;">
								  <div class="card-header">Ajouter un Cours</div>
									  <div class="card-body">

									    <p class="card-text text-white"><button type="button" class="btn btn-info" href="" data-toggle="modal" data-target="#add_cour">Ajouter !</button></p>
								 	  </div>
							</div>

		            	</div>
			            		<?php 
			            	}else if ($conf['estComplet'] == 1) { ?>
			            			
					            	<!--div class="col-sx-3 ">
					            		<div class="card text-black bg-default mb-3 text-center" style="width: 15rem;">
											  <div class="card-header">Lesson</div>
												  <div class="card-body">
												    <p class="card-text text-white"><a type="button" class="btn btn-info" href="function/function?id_tuto=<?php echo $id_tuto ?>&id_chap=<?php echo $id_chap ?>" >Ajouter</a></p>
											 	  </div>
										</div>

					            	</div-->
			            		<?php 
			            	}

			            			?>






		            	           				              <!-- The Modal -->
				  <div class="modal fade" id="add_cour">
				    <div class="modal-dialog modal-lg">
				      <div class="modal-content">
				      
				        <!-- Modal Header -->
				        <div class="modal-header">
				          <h4 class="modal-title">Ajouter un cour</h4>

				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				        </div>
				        
				        <!-- Modal body -->
				        <div class="modal-body">
				        	<span class="text-center text-muted">Rassurer vous de bien remplir les champs plus de modifications</span>
				        <div class="container">
				        	<form method="POST" action="function/function.php">

							       <div class="form-group">
									    <label for="tuto_name">Libélé du cours</label>
									    <textarea type="text" class="form-control" id="cours" name="cours" cols="300" rows="20" required></textarea> 
								  </div>

								  <div class="form-group" style="display: none">
									    <input type="text" class="form-control"  name="id_tuto"  value="<?php echo $id_tuto?>" required></textarea> 
								  </div>

								   <div class="form-group" style="display: none">
									    <input type="text" class="form-control"  name="id_chap"  value="<?php echo $id_chap?>" required></textarea> 
								  </div>
								   <div class="form-group" style="display: none">
									    <input type="text" class="form-control"  name="id_lesson"  value="<?php echo $id_lesson?>" required></textarea> 
								  </div>

								  <h3 class="text-center">type de question</h3>
								  
								  	<div class="row">
										  	<div class="col-sx-3 offset-5">
												  <label><input type="radio" name="optradio" value="qcm"  id="qcm">QCM</label>
											</div>

											<div class="col-sx-3 offset-1">
												  <label><input type="radio" name="optradio" value="qr" id="qr">QR</label>
											</div>

								  	</div>

								  	<div class="container" id="formQCM">
								  		 <h3 class="text-center">Une question type QCM</h3>
								  		  <div class="form-group">
											    <label for="questionQCM">Libele de la question</label>
											    <input type="text" class="form-control" id="questionQCM" name="questionQCM">
											  </div>
											  <div class="form-group">
											    <label for="propo1">Proposition 1</label>
											    <input type="texte" class="form-control" id="propo1" name="propo1">
											  </div>
											   <div class="form-group">
											    <label for="propo2">Proposition 1</label>
											    <input type="texte" class="form-control" id="propo2" name="propo2">
											  </div>

											  <div class="form-group">
											    <label for="pwd">Proposition 3</label>
											    <input type="texte" class="form-control" id="propo3" name="propo3">
											  </div>
											  <div class="form-group">
											    <label for="pwd">Numero de la reponse (1-3) </label>
											    <input type="number" class="form-control" id="reponseQCM" name="reponseQCM">
											  </div>																			  	
								  	</div>

								  	<div class="container" id="formQR">
 											<h3 class="text-center">Question type Reponse</h3>
								  		  <div class="form-group">
											    <label for="questionQCM">Question</label>
											    <input type="text" class="form-control" id="questionQR" name="questionQR">
											  </div>
											  <div class="form-group">
											    <label for="questionQR">reponse</label>
											    <input type="texte" class="form-control" id="reponseQR" name="reponseQR">
											  </div>
																							  	
								  	</div>
								  <div class="offset-6">
								  	 <button type="submit" class="btn btn-primary " name="addCour" value="Ajouter" >Ajouter</button>
								  </div>
								 
						   </form>
						</div>

				        </div>
				        
				        <!-- Modal footer -->
				        <div class="modal-footer">
				          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        </div>
				        
				      </div>
				    </div>
				  </div>


            </div>


	</div>

</div>
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();   
});
</script>
