<?php
		require 'function/connexion_succes.php';
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Quizz</title>
	  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- Bootstrap core CSS -->
  
    <!--link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"-->
    <!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"-->
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


        <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
             <!-- Bootstrap Js CDN -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
           <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
		  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> 
		  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
		  <script type="text/javascript" src="assets/js/javascript.js"></script>
		 
</head>
<body>
	<div class="container-fluid">
			<?php include("include/nav.php"); ?>
		   	<?php include("include/sidebar.php"); ?>
		   	<?php include("include/mainListTuto.php"); ?>

	</div>
	<script type="text/javascript">
		

		//on open collapse
$('.collapse').on('shown.bs.collapse', function () {
  var target = '#'+$(this).attr('data-parent');
  $(target).addClass('collapse-open');
})
	</script>
</body>
</html>