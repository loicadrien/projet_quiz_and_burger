<?php
	// on se connecte a la base de données
		require "database.php";
        extract($_POST);
        $db = Database::connect();
        $req = $db->prepare('SELECT * FROM administrateur WHERE email = :email AND password = :password');
        $req->execute(array(
            'email' => $_POST['email'],
            'password' => sha1($_POST['password'])));

        $existe = $req->fetch();
        if ($existe) {
            session_start();
            $_SESSION['id_admin'] = $existe['id_admin'];
            $_SESSION['email'] = $existe['email'];
            header('Location: ../');

        }else{

            $erreur = 'Mot de passe ou email invalide';
            header('Location: ../connexion.php?error='.$erreur);

        }

?>