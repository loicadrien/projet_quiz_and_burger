<?php

    // insersion d'un tutoriel
        require "database.php";
        extract($_POST);
        $db = Database::connect();
    if(isset($_POST['addTuto'])){
        extract($_POST);
        $intitule = $_POST['tuto_name'];
        $info = $_POST['tuto_info'];

        echo $intitule. " ". $info;

        // on verifi dab si le tuto n'existe pas 
        $req = $db->prepare('SELECT intituler FROM tutoriel WHERE intituler = :intitule');
        $req->execute(array(
            'intitule' => $intitule));

        $existe = $req->fetch();
        if ($existe) {
            header('Location: ../index.php?error=le tutoriel existe déjà');
        }else{
            // on l'insert dans la table tutoriel
        $niv = $nbr_chap = 0;
        $req = $db->prepare('INSERT INTO tutoriel SET intituler =?,info_sup =?,niv=?,nbr_chap=?');
        $req->execute(array($intitule, $info, $niv ,$nbr_chap));
        header("Location: ConfigurerT.php?id=".$db->lastInsertId());
        exit();
        }

    }
// ajo ut d'un chap
    if(isset($_POST['addChap'])){

        extract($_POST);
        $id_tuto = $_POST['id_tuto'];
        $intitule = $_POST['chap_name'];
        $info = $_POST['chap_info'];
        
                // on verifi dab si le chapitre n'existe pas 
        $req = $db->prepare('SELECT intitule FROM chapitre WHERE intitule = :intitule');
        $req->execute(array(
            'intitule' => $intitule));
        $existe = $req->fetch();

        if ($existe) {
            header('Location: ../modifTuto.php?id_tuto='.$id_tuto.'&error=le chapitre existe déjà');
        }else{
         $niv = 0;
        $req = $db->prepare('INSERT INTO chapitre SET intitule =?,info =?, id_tuto=?');
        $req->execute(array($intitule,$info,$id_tuto));
        $last=$db->lastInsertId();
        // on selection le nombre de chapitre se trouvant dans la bd puis on l'update

        $state1 = $db->prepare('SELECT nbr_chap FROM tutoriel WHERE id_tuto=?');
        $state1->execute(array($id_tuto));
        $nbr_chap = $state1->fetch();
        $nbr_chap1 = (int)$nbr_chap;
        $nbr_chap1 +=1;
        // on update le nombre de chapitre de la table tuto
        $state2 = $db->prepare('UPDATE tutoriel set nbr_chap = ? WHERE id_tuto = ?');
        $state2->execute(array($nbr_chap1,$id_tuto));

        header("Location: ConfigurerCa.php?id=".$last."&id_tuto=".$id_tuto);
        exit();
        }


    }

    // on a appuyé sur le button ajouter un cours
    if (isset($_POST['addCour'])) {
       
        $cour = $_POST['cours'];
        $id_tuto = $_POST['id_tuto'];
        $id_chap = $_POST['id_chap'];
        $id_lesson = $_POST['id_lesson'];
        $typeQ = $_POST['optradio'];
        // variable pour qcm
        $questionQCM = $_POST['questionQCM'];
        $propo1 = $_POST['propo1'];
        $propo2 = $_POST['propo2'];
        $propo3 = $_POST['propo3'];
        $reponseQCM = $_POST['reponseQCM'];
         // variable pour QR
         $questionQR = $_POST['questionQR'];
         $reponseQR = $_POST['reponseQR'];

         if($typeQ == "qcm"){

  
            
            // insertion du cours
            $req= $db->prepare("INSERT INTO cours SET textcours = ?, typeQ = ? ,id_lesson = ?");
            $req->execute(array($cour,$typeQ,$id_lesson));
            $last=$db->lastInsertId();

            // insertion du qcm
            $req= $db->prepare("INSERT INTO qcm SET libele = ?, proposition1 = ? , proposition2=?, proposition3 = ?, reponse = ?, id_cours = ?");
            $req->execute(array($questionQCM,$propo1,$propo2,$propo3,$reponseQCM,$last));


         }else if ($typeQ == "qr") {
                        // insertion du cours
            $req= $db->prepare("INSERT INTO cours SET textcours = ?, typeQ = ? ,id_lesson = ?");
            $req->execute(array($cour,$typeQ,$id_lesson));
            $last=$db->lastInsertId();
            print($last);
            print($questionQR);
            print($reponseQR);
            $last1 = (int)$last;
            // insertion du qcm

            $req1= $db->prepare("INSERT INTO qr SET libele = ?, reponse = ?, id_cours = ?");
            $req1->execute(array($questionQR,$reponseQR,$last1));

            
         }else{

             $message = "OUPS";
         }

         // on compte le nombre de cours de la table cours correspondant a la lesson
                 $req = $db->prepare('SELECT count(id_cours) AS nbr_cours FROM cours WHERE id_lesson = ?');
                 $req->execute(array($id_lesson));
                 $count = $req->fetch();
                 $req->closeCursor();
                 $nbr_cour = (int)$count['nbr_cours'];
                 $nbr_cour +=1;

        // on selectionne le champ nombre de cours 
                    $state2 = $db->prepare('SELECT nbr_cours FROM lesson WHERE id_chap=? AND id_lesson=?');
                    $state2->execute(array($id_chap,$id_lesson));
                    $nbr_crs = $state2->fetch();
                    $nbr_crs = $nbr_cour;

        // on update la table lesson
                        $state3 = $db->prepare('UPDATE lesson set nbr_cours = ? WHERE id_chap = ? AND id_lesson=?');
                        $state3->execute(array($nbr_crs,$id_chap,$id_lesson));
                        header("Location: ../modifChap.php?id_tuto=".$id_tuto."&id_chap=".$id_chap);
        //on update la table configurerT
                
                        $estPret = 1;
                        $state3 = $db->prepare('UPDATE configurerT set estPret = ? WHERE id_tuto = ?');
                        $state3->execute(array($estPret,$id_tuto));
                        header("Location: ../modifChap.php?id_tuto=".$id_tuto."&id_chap=".$id_chap);

         


        header("Location: ../modifLesson.php?id_tuto=".$id_tuto."&id_chap=".$id_chap."&id_lesson=".$id_lesson."&message=".$message);
    }

    if(isset($_GET['id_tuto']) && isset($_GET['id_chap']))
{
     $id_tuto = trim($_GET['id_tuto']);
     $id_tuto = stripslashes($_GET['id_tuto']);
     $id_tuto = htmlspecialchars($_GET['id_tuto']);

                 $id_chap = trim($_GET['id_chap']);
                 $id_chap = stripslashes($_GET['id_chap']);
                 $id_chap = htmlspecialchars($_GET['id_chap']);

                 // on compte le nombre de lessons qui se trouve dans la bd

                 $req = $db->prepare('SELECT count(id_lesson) AS nbr_lesson FROM lesson WHERE id_chap = ?');
                 $req->execute(array($id_chap));
                 $count = $req->fetch();
                 $req->closeCursor();
                 $nbr_lesson = (int)$count['nbr_lesson'];
                 $nbr_lesson +=1;
                 $lesson = "Leçon ".$nbr_lesson;

                 $state1 = $db->prepare("INSERT into lesson SET intitule=?,id_chap=?");
                 $state1->execute(array($lesson,$id_chap));
                 
                   

                    $state2 = $db->prepare('SELECT nbr_lesson FROM chapitre WHERE id_tuto=? AND id_chap=?');
                    $state2->execute(array($id_tuto,$id_chap));
                    $nbr_les = $state2->fetch();
                    $nbr_les = $nbr_lesson;
                   
                    

                                // on update le nombre de lesson de la table chapitre
                        $state3 = $db->prepare('UPDATE chapitre set nbr_lesson = ? WHERE id_tuto = ? AND id_chap=?');
                        $state3->execute(array($nbr_les,$id_tuto,$id_chap));
                        header("Location: ../modifChap.php?id_tuto=".$id_tuto."&id_chap=".$id_chap);

}



    // je sais plus trop a quoi ca sert

    if(isset($_GET['id_tuto']) && isset($_GET['id_chap']) && isset($_GET['id_lesson']))
        {
                 $id_tuto = trim($_GET['id_tuto']);
                 $id_tuto = stripslashes($_GET['id_tuto']);
                 $id_tuto = htmlspecialchars($_GET['id_tuto']);

                 $id_chap = trim($_GET['id_chap']);
                 $id_chap = stripslashes($_GET['id_chap']);
                 $id_chap = htmlspecialchars($_GET['id_chap']);

                 $id_lesson = trim($_GET['id_lesson']);
                 $id_lesson = stripslashes($_GET['id_lesson']);
                 $id_lesson = htmlspecialchars($_GET['id_lesson']);



        }
        // buton mise en ligne
        if(isset($_GET['id_tutoF'])){
                 $id_tuto = trim($_GET['id_tutoF']);
                 $id_tuto = stripslashes($_GET['id_tutoF']);
                 $id_tuto = htmlspecialchars($_GET['id_tutoF']);

                                                 // on update le nombre de lesson de la table chapitre
                        $state = $db->prepare('UPDATE configurert set estComplet = ? WHERE id_tuto = ?');
                        $state->execute(array((int)1,$id_tuto));
                        header("Location: ../");



        }


        // delete une leçon
         if(isset($_GET['id_tutoD']) && isset($_GET['id_chapD']) && isset($_GET['id_lessonD']))
        {
                 $id_tuto = trim($_GET['id_tutoD']);
                 $id_tuto = stripslashes($_GET['id_tutoD']);
                 $id_tuto = htmlspecialchars($_GET['id_tutoD']);

                 $id_chap = trim($_GET['id_chapD']);
                 $id_chap = stripslashes($_GET['id_chapD']);
                 $id_chap = htmlspecialchars($_GET['id_chapD']);

                 $id_lesson = trim($_GET['id_lessonD']);
                 $id_lesson = stripslashes($_GET['id_lessonD']);
                 $id_lesson = htmlspecialchars($_GET['id_lessonD']);

                 // on suprime la lecon correspondante

                 $state = $db->prepare('DELETE FROM lesson WHERE id_lesson = ?');
                 $state->execute([$id_lesson]);

                 // on 
                $req = $db->prepare('SELECT count(id_lesson) AS nbr_lesson FROM lesson WHERE id_chap = ?');
                 $req->execute(array($id_chap));
                 $count = $req->fetch();
                 $req->closeCursor();
                 $nbr_lesson = (int)$count['nbr_lesson'];

                    $state2 = $db->prepare('SELECT nbr_lesson FROM chapitre WHERE id_tuto=? AND id_chap=?');
                    $state2->execute(array($id_tuto,$id_chap));
                    $nbr_les = $state2->fetch();
                    $nbr_les = $nbr_lesson;
                   
                    

                                // on update le nombre de lesson de la table chapitre
                        $state3 = $db->prepare('UPDATE chapitre set nbr_lesson = ? WHERE id_tuto = ? AND id_chap=?');
                        $state3->execute(array($nbr_les,$id_tuto,$id_chap));
                        header("Location: ../modifChap.php?id_tuto=".$id_tuto."&id_chap=".$id_chap);
        }

        // enregistrement d'un new administrateur
        if(isset($_POST['register']))
        {

            $nomErreur=$emailErreur=$passwordErreur=$FileErreur="";
            extract($_POST);
            $nom = $_POST['nom'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            $file = checkInput($_FILES["image"]['name']);

          /// print_r(array($_POST, $_FILES));

            $filePath = '../image/'.basename($file);
            $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);
            $isSuccess = true;
            $isUploadSuccess = false;

                          //Verification du nom
                if(!preg_match('/^[a-z A-Z]+$/',$nom) || empty($nom)){
                    $nomErreur = "Nom invalide (alphanumerique)";
                    $isSuccess = false;

                }

                //Verification d' email
                if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                    $emailErreur= "Format d'email invalide";
                    $isSuccess = false;
                }
                else{

                    $req = $db->prepare('SELECT email FROM administrateur WHERE email = ?');
                    $req->execute(array($email));
                    $user = $req->fetch();
                    if($user){
                        $emailErreur = 'Cet email existe déja';
                        $isSuccess = false;
                    }
                }
                                        //Verification du password
                if(strlen($password)<8 || empty($nom)){
                    $passwordErreur = "le password invalide";
                    $isSuccess = false;

                }

                                        // controle sur l'image
                        if(empty($file)){
                            $FileError = 'ichamps image vide';
                            $isSuccess = false;
                        }else{
                            $isUploadSuccess = true;
                            if($fileExtension != "jpg" && $fileExtension != "png" && $fileExtension != "gif" && $fileExtension != "jpeg"){
                                $FileError = "file non autorisés ";
                                $isUploadSuccess = false;

                            }if(file_exists($filePath)){
                                $FileError = "file exist";
                                $isUploadSuccess = false;
                            }if($_FILES['image']["size"] > 500000)
                            {
                                $FileError = "file trop grand ";
                                $isUploadSuccess = false;
                            }if($isUploadSuccess){
                                if(!move_uploaded_file($_FILES['image']["tmp_name"], $filePath)){
                                    $FileError = "upload error ";
                                    $isUploadSuccess = false;
                                }
                            }
                        }

                                            //Insertion lorsqu'il n'ya pas d"erreurs
                    if(($isSuccess == true && $isUploadSuccess == true)){
                        $req = $db->prepare('INSERT INTO administrateur SET nom =?,email=?,password=?,image=?');
                        $mat = sha1($password);
                        $req->execute(array($nom,$email,$mat,$file));

                        header("Location: ../index.php");

                    }else{
                         header("Location: ../addAdmin.php?erreur=Des erreurs sont survenues :".$nomErreur." /".$passwordErreur." /".$emailErreur." /".$FileError);
                    }

        }

        if(isset($_POST['updateTuto'])){
            extract($_POST);
            $intituler = checkInput($_POST['intituler']);
            $info_sup = checkInput($_POST['info']);
            $id_tuto = checkInput($_POST['id_tuto']);

                                            // on update le nombre de lesson de la table chapitre
                $state3 = $db->prepare('UPDATE tutoriel set intituler = ?, info_sup = ? WHERE id_tuto = ?');
                $state3->execute(array($intituler,$info_sup,$id_tuto));


        header("Location: ../index.php");
    }
    if (isset($_POST['updateChap'])) {
                    extract($_POST);
            $intituler = checkInput($_POST['intituler']);
            $info_sup = checkInput($_POST['info']);
            $id_chap = checkInput($_POST['id_chap']);

                 // on update le nombre de lesson de la table chapitre
                $state3 = $db->prepare('UPDATE chapitre set intitule = ?, info = ? WHERE id_chap = ?');
                $state3->execute(array($intituler,$info_sup,$id_chap));


        header("Location: ../index.php");

        
    }

    if(isset($_POST['registerUser'])){
        extract($_POST);
        $nomErreur=$prenomErreur=$emailErreur=$passwordErreur=$FileErreur=$passwordconfirmErreur="";

        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $file = checkInput($_FILES["image"]['name']);

        /// print_r(array($_POST, $_FILES));
        $filePath = '../../users/img/'.basename($file);
        $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);
        $isSuccess = true;
        $isUploadSuccess = false;

        //Verification du nom
        if(!preg_match('/^[a-z A-Z]+$/',$nom) || empty($nom)){
            $nomErreur = "Nom invalide (alphanumerique)";
            $isSuccess = false;

        }
        //Verification d'email
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $emailErreur= "Format d'email invalide";
            $isSuccess = false;
        }
        else{
            $req = $db->prepare('SELECT email FROM utilisateur WHERE email = ?');
            $req->execute(array($email));
            $user = $req->fetch();
            if($user){
                $emailErreur = 'Cet email existe déja';
                $isSuccess = false;
            }
        }
        //Verification du password
        if(strlen($password)<8 || empty($password)){
            $passwordErreur = "le password invalide";
            $isSuccess = false;
        }


        // controle sur l'image
        if(empty($file)){
            $FileError = 'champs image vide';
            $isSuccess = false;
        }else{
            $isUploadSuccess = true;
            if($fileExtension != "jpg" && $fileExtension != "png" && $fileExtension != "gif" && $fileExtension != "jpeg"){
                $FileError = "file non autorisés ";
                $isUploadSuccess = false;

            }if(file_exists($filePath)){
                $FileError = "file exist";
                $isUploadSuccess = false;
            }if($_FILES['image']["size"] > 500000)
            {
                $FileError = "file trop grand ";
                $isUploadSuccess = false;
            }if($isUploadSuccess){
                if(!move_uploaded_file($_FILES['image']["tmp_name"], $filePath)){
                    $FileError = "upload error ";
                    $isUploadSuccess = false;
                }
            }
        }

        //Insertion lorsqu'il n'ya pas d"erreurs
        if(($isSuccess == true && $isUploadSuccess == true)){
            $req = $db->prepare('INSERT INTO utilisateur SET nom =?,prenom = ?,email=?,password=?,points=?,image=?,active=?');
            $mat = sha1($password);
            $req->execute(array($nom,$prenom,$email,$mat,0,$file,1));
            echo "ok";
            header("Location: ../index.php");

        }else{
            echo "erreur bizarre";
            header("Location: ../addUser.php?erreur=Des erreurs sont survenues :".$nomErreur." ".$prenomErreur." ".$passwordErreur." ".$emailErreur." ".$FileError);
        }
    }

        function checkInput($data)
        {
            $data = trim($data);
            $data = stripcslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

?>