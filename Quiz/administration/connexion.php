    <?php
 $erreur="";
if(!empty($_GET['error'])){
    $erreur = trim($_GET['error']);
     $erreur = stripslashes($_GET['error']);
     $erreur = htmlspecialchars($_GET['error']);
 }
  ?>
<!DOCTYPE html>
<html>
<head>
    <title>Quizz</title>
      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap core CSS -->
  
    <!--link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"-->
    <!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"-->
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


        <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
             <!-- Bootstrap Js CDN -->
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
           <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> 
          <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
</head>

   <body class="bg-dark">
    

        <div class="container py-5">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center text-white mb-4">Connexion administrateur</h2>
                    <div class="row">
                        <div class="col-md-6 mx-auto">
                            <span class="anchor" id="formLogin"></span>

                            <!-- form card login -->
                            <div class="card rounded-0">
                                <div class="card-header">
                                    <h3 class="mb-0">Login</h3>
                                </div>
                                <div class="card-body">
                                    <form class="form" role="form" action="function/connexion.php" method="POST" autocomplete="off" id="formLogin">
                                        <div class="form-group">
                                            <label for="uname1">email</label>
                                            <input type="email" class="form-control form-control-sm rounded-0" name="email" id="email" required="">
                                        </div>
                                        <div class="form-group">
                                            <label>mot de passe</label>
                                            <input type="password" class="form-control form-control-sm rounded-0" name="password" id="pwd" required="" autocomplete="new-password">
                                        </div>
                                        <div>
                                            <label class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input">
                                              <span class="custom-control-indicator"></span>
                                              <span class="custom-control-description small text-warning text-center"><?php echo $erreur; ?></span>
                                            </label>
                                        </div>
                                        <button type="submit" class="btn btn-success btn-lg float-right">Login</button>
                                    </form>
                                </div>
                                <!--/card-block-->
                            </div>
                            <!-- /form card login -->

                        </div>


                    </div>
                    <!--/row-->

               
         </div>
                <!--/col-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->



    </body>
</html>
